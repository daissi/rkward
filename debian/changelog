rkward (0.8.0-1) UNRELEASED; urgency=medium

  * New upstream version 0.8.0

 -- Dylan Aïssi <daissi@debian.org>  Thu, 30 Jan 2025 21:30:48 +0100

rkward (0.7.5-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Build with QtWebEngine. (Closes: #1093381)

 -- Bastian Germann <bage@debian.org>  Sat, 18 Jan 2025 23:47:39 +0100

rkward (0.7.5-3) unstable; urgency=medium

  * Team upload.
  * Backport upstream commit b53d754f18bef786e92f266f576afdbe5fd7d00b, and
    commit ded0472b90c398adc05a7fed1b6446417789ba58 to build with newer versions
    of R; patches upstream_Fix-not-a-string-literal-compile-error.patch, and
    upstream_Fix-compilation.patch. (Closes: #1071360)
  * Bump Standards-Version to 4.7.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Tue, 28 May 2024 07:28:08 +0200

rkward (0.7.5-2) unstable; urgency=medium

  * Team upload.
  * Depend also on the R graphics engine version: (Closes: #1058801)
    - query the r-graphics-engine-N provide from r-base-core
    - add it as dependency via substvars
  * Update standards version to 4.6.2, no changes needed.
  * Drop the breaks/replaces for versions older than what is in Debian Bullseye
    (i.e. current oldstable).

 -- Pino Toscano <pino@debian.org>  Sat, 16 Dec 2023 20:23:16 +0100

rkward (0.7.5-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * This new version has tests that require a display, so:
    - add the xvfb, and xauth build dependencies
    - run dh_auto_test via xvfb-run
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Sat, 22 Oct 2022 13:06:04 +0200

rkward (0.7.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update watch file.
  * Update standards version to 4.6.1, no changes needed.
  * Update the build dependencies according to the upstream build system:
    - add qtdeclarative5-dev
    - drop qtscript5-dev

 -- Pino Toscano <pino@debian.org>  Tue, 31 May 2022 08:44:51 +0200

rkward (0.7.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release:
    - builds with newer versions of R (Closes: #1008322)
  * Update watch file format version to 4.
  * Update standards version to 4.6.0, no changes needed.
  * Update the build dependencies according to the upstream build system:
    - bump cmake to 3.10.0
    - bump the Qt5 package to 5.9.0
    - bump the KF5 packages to 5.44.0
    - add libkf5archive-dev
  * debian/upstream/metadata: improve Repository* keys.
  * Do not try to remove R.css, and COPYING from the installed files, as they
    are no nore shipped.
  * Rename docs to rkward.docs to make it clear to which package it refers.
  * Move handling of the r-base-core & r-api substvars directly when calling
    dh_gencontrol, rather than adding them to rkward.substvars; simplify rules
    a bit, and runs the dpkg commands only when & where needed.

 -- Pino Toscano <pino@debian.org>  Sun, 24 Apr 2022 20:04:10 +0200

rkward (0.7.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update the cmake build flags:
    - pass -DNO_QT_WEBENGINE=1 to keep using KDEWebKit for now
    - drop -DNO_R_XML=1, no more needed now
    - drop -DBUILD_TIMESTAMP, no more used since 0.7.1; because of this, stop
      including /usr/share/dpkg/pkg-info.mk
  * Drop removal of x-test translations, as they are no more shipped.
  * Bump the debhelper compatibility to 13:
    - switch the debhelper-compat build dependency to 13
  * Update install files.
  * Fix day-of-week for changelog entries 0.6.1-1, 0.6.0-3, 0.5.6-1, 0.5.5-2,
    0.5.0d-3, 0.5.0d-2, 0.5.0c-1, 0.4.8a-2, 0.4.2-1.

 -- Pino Toscano <pino@debian.org>  Fri, 16 Oct 2020 11:25:10 +0200

rkward (0.7.1b-2) unstable; urgency=medium

  * Team upload.
  * Update the build dependencies according to the upstream build system:
    - drop the pkg-kf5-tools alternative, as it never existed
    - add the version required for cmake, and the KF5 packages
    - explicitly add gettext, libkf5config-dev, libkf5coreaddons-dev,
      libkf5crash-dev, libkf5i18n-dev, libkf5notifications-dev,
      libkf5parts-dev, libkf5xmlgui-dev, libkf5widgetsaddons-dev,
      libkf5windowsystem-dev, libqt5webkit5-dev, qtbase5-dev, and qtscript5-dev
  * Move the application desktop file, and the appdata XML file from
    rkward-data to rkward, as they must be together with the executable
    they refer to
    - add proper breaks/replaces
    - remove no more needed lintian override
  * Remove rkward-data.manpages, as the man page is already installed using
    dh_install.
  * Do not install the upstream TODO as documentation, as it is not useful for
    users.
  * Stop forcing debian/build as local build directory, as there is no need to
    have a fixed name; use the default one instead.
  * Use the kf5 dh addon for the build
    - remove the need for the manual dh_compress handling
  * Remove the CFLAGS/CXXFLAGS handling, as it is done by the cmake buildsystem
    in debhelper compatibility >= 9.
  * Stop passing --parallel to dh_auto_configure, as it is the default with
    debhelper compatibility >= 10.
  * Drop all the pre-Stretch replaces/breaks.
  * Pass -DNO_R_XML=1 to cmake to not install the r.xml highlight file for the
    syntax-highlighting framework, as the syntax-highlighting already provides
    it
    - drop the old removal hack (which got broken lately) for it

 -- Pino Toscano <pino@debian.org>  Fri, 19 Jun 2020 13:05:22 +0200

rkward (0.7.1b-1) unstable; urgency=medium

  * New upstream version.

 -- Dylan Aïssi <daissi@debian.org>  Mon, 25 May 2020 21:02:54 +0200

rkward (0.7.1-2) unstable; urgency=medium

  * Update VCS fields to target the Debian KDE Extras Team namespace.
  * Rules-Requires-Root: no (routine-update)
  * Simplify dependency of the r-api-*

 -- Dylan Aïssi <daissi@debian.org>  Sat, 16 May 2020 00:03:57 +0200

rkward (0.7.1-1) unstable; urgency=medium

  * New upstream version.
  * Remove d/p/fix_includes.diff included upstream.
  * Switch Maintainer to Debian KDE Extras Team,
     add Thomas Friedrichsmeier and myself to Uploaders.
  * Add license of rkward/org.kde.rkward.appdata.xml in d/copyright.
  * Bump year to 2020 in d/changelog.
  * Remove trailing whitespace in d/changelog.
  * Switch to debhelper-compat (= 12).
  * Remove an unused override in rkward-data.
  * Bump standards version to 4.5.0 (no changes needed).
  * Remove the override about .Rdata and document it in d/README.source.
  * Fix broken links to documentation for dh_compress.
  * Remove DR_LIBDIR in dh_auto_configure, not used anymore.
  * Add debian/upstream/metadata.
  * Add debian/salsa-ci.yml.
  * Update VCS fields to salsa.

 -- Dylan Aïssi <daissi@debian.org>  Fri, 10 Apr 2020 14:43:17 +0200

rkward (0.7.0b-2) unstable; urgency=medium

  * Add missing includes that cause FTBFS. Closes: 948939
  * Add upstream contact to debian/copyright
  * Adjust homepage url to use https:// instead of http://
  * Bump standards version to 4.4.1 (no changes needed)

 -- Thomas Friedrichsmeier <thomas.friedrichsmeier@kdemail.net>  Fri, 17 Jan 2020 20:30:00 +0100

rkward (0.7.0b-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix shell error in debian/rules. Closes: #911710

 -- Christoph Biedl <debian.axhn@manchmal.in-ulm.de>  Sun, 28 Oct 2018 12:14:47 +0100

rkward (0.7.0b-1) unstable; urgency=low
  * New upstream release
    Includes missing translation files in rkward-data
  * Replace kdoctools-dev dependency with libkf5doctools-dev
    closes: #895875
  * Depend on r-api-3.5 when built against r-base-core >= 3.5.0
  * Do not depend on r-api-3.4 when built against r-base-core < 3.4.0
  * Remove explicit specification of C compiler
    closes: #900591

 -- Thomas Friedrichsmeier <thomas.friedrichsmeier@kdemail.net>  Sat, 02 Jun 2018 09:00:00 +0100

rkward (0.7.0-1) unstable; urgency=low
  * New upstream release
  * The divergently licenced qwinhost-files are no longer part of this release,
    simplifying copyright
  * Package now based on kf5-libraries, instead of KDE 4
    closes: #531086
  * Add dependency on r-api-3.4 to ease handling of future non-compatible
    changes in R
    closes: #877283
  * Use dh_auto_configure for basic setup
  * Remove debian/rkward.menu, as package provides a FreeDesktop entry
  * Update VCS-browser, watch, and some other urls
  * Update standards version to 4.1.4
  * Add lintian overrides for r-data-without-readme-source and
    desktop-command-not-in-package
  * Make rkward binary package depend on identical version rkward-data package
  * Increase debhelper compat level to 11
  * Use SOURCE_DATE_EPOCH for build timestamp, instead of parsing changelog

 -- Thomas Friedrichsmeier <thomas.friedrichsmeier@kdemail.net>  Sat, 14 Apr 2018 13:00:00 +0100

rkward (0.6.5-1) unstable; urgency=low
  * new upstream release
    (unchanged with respect to ~rc1)
  * fix debian/watch file
  * always use build-time version of r-base-core as runtime dependency, even
    if most builds would actually be backwards compatible
  * switch to dh-based rules file. This also fixes missing separation between
    arch-dependent and arch-independent build targets
  * Make build really reproducible (fix for earlier attempt upstream)
    closes: #783290

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Mon, 04 Apr 2016 11:00:00 +0100

rkward (0.6.5~rc1-1) unstable; urgency=low
  * new upstream release
  * split architecture independent files into new rkward-data package
  * bump standards version to 3.9.7 (no changes needed)
  * strip testing locale "x-test" from package
  * add override for false-positive source-contains-prebuilt-javascript-object
    lintian warnings
  * make existing lintian overrides work with lintian 2.5.42
  * Adjust copyright file to changed filenames in rkward/qwinhost

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Tue, 29 Mar 2016 11:00:00 +0100

rkward (0.6.4-1) unstable; urgency=low
  * new upstream release
  * add override for false-positive source-is-missing lintian warnings
  * remove obsolete README.Debian file
  * increase debhelper compatibility level to 9 (no changes needed)
  * replace dh_clean -k with dh_prep
  * remove legacy rules dealing with R versions prior to 3.0.0
  * upstream has replaced KHTMLPart with QWebKit. As Qt4WebKit is scheduled for
    removal, revert this change, here. Change to 3.0 (quilt) format for this.
  * Make build reproducible (thanks to Philip Rinn)
    closes: #783290

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Wed, 16 Dec 2015 10:30:00 +0100

rkward (0.6.3-1) unstable; urgency=low
  * new upstream release
  * Ajdust to move of homepage to rkward.kde.org, and vcs to git.kde.org

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Sat, 07 Mar 2015 13:30:00 +0100

rkward (0.6.2~rc1-2) unstable; urgency=low

  * bump standards version to 3.9.6 (no changes needed)

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Fri, 17 Oct 2014 10:30:00 +0100

rkward (0.6.2~rc1-1) unstable; urgency=low

  * new upstream release
  * avoid duplicate cmake run during build
  * upstream increased lowest supported R version to 2.8.0
  * bump standards version to 3.9.4 (no changes needed)

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Thu, 16 Oct 2014 13:30:00 +0100

rkward (0.6.1-1) unstable; urgency=low

  * upstream release 0.6.1-rc1 was made available as 0.6.1 without changes
  * new upload to build against R 3.0.0 on most architectures
  * when built against r-base-core >= 3.0.0, depend on r-base-core <= 3.0.0

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Tue, 07 May 2013 12:30:00 +0100

rkward (0.6.1~rc1-1) unstable; urgency=low

  * new upstream release
  * re-add Vcs-Browser field
  * fix year in changelog (previous upload had 2012 instead of 2013)
  * add runtime dependency on r-base-core < 3.0.0 unless compiled with >= 3.0.0

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Thu, 28 Mar 2013 09:30:00 +0100

rkward (0.6.0-3) unstable; urgency=low

  * correctly treat -pre releases in debian/watch (thanks to Bart Martens)
  * remove obsolete DM-Upload-Allowed field
  * update Vcs-Svn field to changed repository URL
  * remove Vcs-Browser field, as there is no useful repository browser, ATM

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Sun, 15 Jan 2012 12:30:00 +0100

rkward (0.6.0-2) unstable; urgency=low

  * make sure to include CPPFLAGS and LDFLAGS in call to cmake
  * correct path to qwinhost files in debian/copyright
  * correct syntax in debian/copyright (thanks to Laszlo Kajan)

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Sat, 24 Nov 2012 14:15:00 +0200

rkward (0.6.0-1) unstable; urgency=low

  * new upstream release
  * remove support for building on Ubuntu hardy
  * more accurate copyright file
    closes: #689982
  * bump standards version to 3.9.4 (no changes needed)
  * Add Vcs-Browser and Vcs-Svn fields

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Wed, 24 Oct 2012 15:30:00 +0200

rkward (0.5.7-2) unstable; urgency=low

  * new upload to force rebuild against R 2.14.0~x on i386
    closes: #646047 (failure to load rkward package)
  * add dependency on r-base-core < 2.14 if built against R < 2.14.0

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Tue, 25 Oct 2011 14:30:00 +0200

rkward (0.5.7-1) unstable; urgency=low

  * new upstream release
  * provide build-arch and build-indep targets
  * include buildflags as instructed by the dpkg developers

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Sun, 23 Oct 2011 15:30:00 +0200

rkward (0.5.6-1) unstable; urgency=low

  * new upstream release
  * remove empty directory removal rkwardtests/inst. Now fixed upstream.
  * remove man page links to rkward.bin and rkward.rbackend. These
    auxiliary binaries are no longer installed in the system path.
  * remove debian man page. Man page is now supplied upstream.
  * drop explicit dependency on libphonon-dev.
  * fix lintian warning "description-synopsis-starts-with-article"
  * bump standards version to 3.9.2 (no changes needed)

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Mon, 30 May 2011 15:30:00 +0200

rkward (0.5.5-2) unstable; urgency=low

  * Remove empty directory rkwardtests/inst
    fixes FTBFS with (the current alpha of) R 2.13.0

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Sun, 20 Mar 2011 14:30:00 +0200

rkward (0.5.5-1) unstable; urgency=low

  * new upstream release
  * add man-page link for new binary rkward.rbackend

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Tue, 15 Mar 2011 14:30:00 +0200

rkward (0.5.4-1) unstable; urgency=low

  * new upstream release
  * bump standards version to 3.9.1 (no changes needed)
  * no more need to remove svncopy.sh-script in rules

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Mon, 04 Oct 2010 14:30:00 +0200

rkward (0.5.3-2) unstable; urgency=low

  * correct usage of hyphens vs. minus signs in the man page
  * correct the watch file

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Fri, 07 May 2010 14:30:00 +0200

rkward (0.5.3-1) unstable; urgency=low

  * new upstream release
    closes: #556321 (failed to link against thread-library, explicitly)
  * add watch file (thanks to Julien Lavergne and Scott Howard)
    closes: #569114
  * remove svncopy.sh-script included in upstream release tarball by accident
  * remove obsolete runtime dependency on php5-cli
  * add dependency on ${misc:Depends} as recommended by debhelper
  * bump standards version to 3.8.4 (no changes needed)
  * add Homepage field

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Fri, 30 Apr 2010 14:30:00 +0200

rkward (0.5.2-1) unstable; urgency=low

  * new upstream release
    closes: #551306 (added support for the new dynamic help system)
  * Add "DM-Upload-Allowed: yes" in control
  * bump standards version to 3.8.3 (no changes needed)

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Mon, 26 Oct 2009 14:30:00 +0200

rkward (0.5.1-2) unstable; urgency=low

  * drop alternative depend on php4-cli as this is no longer part of any
    current debian based distribution
  * more accurate detection of patch-needing Ubuntu
    Hardy systems (thanks to Meik Michalke)

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Tue, 18 Aug 2009 14:30:00 +0200

rkward (0.5.1-1) unstable; urgency=low

  * new upstream release
  * bump standards version to 3.8.2 (no changes needed)
  * dropped notice on development status from description
  * added notice on differing copyright and additional
    LGPL licence of some files
  * better version detection (thanks to Meik Michalke)
  * added conditional patch to support Ubuntu hardy (thanks to Meik Michalke)
  * decrease debhelper compat level to 6, in order to support Ubuntu hardy
  * revert to using dh_clean -k in order to comply with compat level 6

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Tue, 04 Aug 2009 14:30:00 +0200

rkward (0.5.0d-3) unstable; urgency=low

  * build-depend on libx11-dev
    closes: #529024
  * use dh_prep instead of deprecated dh_clean -k

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Tue, 19 May 2009 14:30:00 +0200

rkward (0.5.0d-2) unstable; urgency=low

  * bumped standards version to 3.8.1
  * increased debhelper compat-level to 7
  * upload to unstable (repeat closes because 0.5.0d-1 was not uploaded)
    closes: #520969
    closes: #491110
    closes: #501649
    closes: #527649 (included missing Rdevices.h; actually fixed in 0.5.0c)

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Mon, 11 May 2009 14:30:00 +0200

rkward (0.5.0d-1) unstable; urgency=low

  * new upstream release
  * upload to unstable (repeat closes because 0.5.0c-1 was not uploaded)
    closes: #520969
    closes: #491110
    closes: #501649

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Sun, 10 May 2009 21:30:00 +0200

rkward (0.5.0c-1) experimental; urgency=low

  * new upstream release
    closes: #520969 (added support for R 2.9.0)
  * query kde4-config to set the correct installation paths
    closes: #491110 (resource files were not found)
    closes: #501649 (plugin files were not found)

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Mon, 30 Mar 2009 20:30:00 +0200

rkward (0.5.0b-1) unstable; urgency=low

  * new upstream release
    closes: #475175
    closes: #463348
    closes: #475982
    closes: #455709

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Sun, 20 Apr 2008 21:30:00 +0200

rkward (0.5.0a-1) experimental; urgency=low

  * new upstream release
  * remove some obsolete comments in rules

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Wed, 23 Jan 2008 16:30:00 +0200

rkward (0.5.0-1) experimental; urgency=low

  * new upstream release
    closes: #455709
  * adjusted to build with KDE4 libraries / CMake

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Mon, 21 Jan 2008 20:30:00 +0200

rkward (0.4.9a-1) unstable; urgency=low

  * new upstream release
    closes: #475175
    closes: #463348
    closes: #475982

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Sun, 20 Apr 2008 21:30:00 +0200

rkward (0.4.9-2) unstable; urgency=low

  * remove --ignore-missing-info parameter again (dpkg-shlibdeps 1.14.12
    does not need it, anymore)
  * when uploaded to main archive, this
    closes: #460745
  * bumped standards to 3.7.3 (no changes necessary)

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Wed, 16 Jan 2008 17:30:00 +0200

rkward (0.4.9-1) unstable; urgency=low

  * new upstream release
  * add --ignore-missing-info paramater to dpkg-shlibdeps, as libR.so will not be found

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Mon, 14 Jan 2008 20:30:00 +0200

rkward (0.4.8a-2) unstable; urgency=low

  * quote the R CMD config calls, as they may also contain compiler arguments
    (thanks to Meik Michalke for a patch)

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Wed, 14 Nov 2007 13:30:00 +0200

rkward (0.4.8a-1) unstable; urgency=low

  * new upstream release
  * document --disable-stack-check and --debugger options in the manual page
  * use the compilers specified by R CMD config
    closes: #450618

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Sun, 11 Nov 2007 13:30:00 +0200

rkward (0.4.8-1) unstable; urgency=low

  * new upstream release
    closes: #417519
  * adjust rkward.menu to new naming policy

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Wed, 03 Oct 2007 13:30:00 +0200

rkward (0.4.7a-2) unstable; urgency=low

  * force gcc 4.2 to match r-base-core
    closes: #432377
  * merge upstream fix for R 2.6
    closes: #442059
  * changed distclean rule in debian/rules to only ignore missing Makefiles

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Fri, 14 Sep 2007 13:30:00 +0200

rkward (0.4.7a-1) unstable; urgency=low

  * new upstream release
    closes: #422541
  * the code to generate the correct r-base-core dependencies should now be POSIX shell compliant
  * remove unused (and uncommented) dh_* scripts from rules

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Mon, 07 May 2007 13:30:00 +0200

rkward (0.4.7-2) unstable; urgency=low

  * explicitely create installation directory for rkward R package

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Wed, 11 Apr 2007 15:30:00 +0200

rkward (0.4.7-1) unstable; urgency=low

  * new upstream release
    closes: #415561 (filename completion)
    closes: #418686 (failure to work with R 2.5.0)

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Wed, 11 Apr 2007 13:30:00 +0200

rkward (0.4.6-1) unstable; urgency=low

  * new upstream release
  * update r-base-core dependencies

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Thu, 15 Feb 2007 19:30:00 +0200

rkward (0.4.5-1) unstable; urgency=low

  * new upstream release
  * provide entry in debian menu (Apps/Math)
    closes: #405376

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Sun, 21 Jan 2007 19:00:00 +0200

rkward (0.4.2-3) unstable; urgency=low

  * this version is not meant to be uploaded to the main archives!
  * fix compilation on sarge

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Wed, 13 Dec 2006 17:30:00 +0200

rkward (0.4.2-2) unstable; urgency=low

  * migrate to unstable

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Wed, 06 Dec 2006 20:00:00 +0200

rkward (0.4.2-1) experimental; urgency=low

  * new upstream release

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Mon, 04 Dec 2006 16:45:00 +0200

rkward (0.4.1-1) unstable; urgency=low

  * new upstream release
  * depend on either php4-cli or php5-cli

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Mon, 06 Nov 2006 16:30:00 +0200

rkward (0.4.0-2) unstable; urgency=low

  * fix automatic build on 64bit architectures
    closes: #394112

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Fri, 20 Oct 2006 14:00:00 +0200

rkward (0.4.0-1) unstable; urgency=low

  * new upstream release
  * remove obsoleted workaround for installation of rkward R package
  * cope with switch from CVS to SVN

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Tue, 17 Oct 2006 14:40:00 +0200

rkward (0.3.7-1) unstable; urgency=low

  * new upstream release
  * use confiure option --with-r-libdir to make sure the rkward R package is installed in /usr/lib/R/site-library/

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Sun, 17 Sep 2006 16:40:00 +0200

rkward (0.3.6-4) unstable; urgency=low

  * fix automatic build on alpha machines
    patch provided by Martin Michlmayr <tbm@cyrius.com>
    closes: #374005
  * bumbed to standards 3.7.2

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Sun, 18 Jun 2006 16:25:00 +0200

rkward (0.3.6-3) unstable; urgency=low

  * first version to be uploaded to debian
    closes: #333392
  * Added Homepage to control file (changed by R.M.Rutschmann <rudi@debian.org>)
  * fix detection of r-base-core dependency when compiled with r-base-core >= 2.3.0
  * clean up rules somewhat

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Tue, 25 Apr 2006 12:45:00 +0200

rkward (0.3.6-2) unstable; urgency=low

  * link manpage directly in installation instead of debian-dir
  * use --show-format instead of -f in call to dpkg-query (to work on stable)
  * delete any debian/CVS subdir included by accident during clean
  * add newline to copyright file

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Mon, 24 Apr 2006 14:45:00 +0200

rkward (0.3.6-1) unstable; urgency=low

  * new upstream release
  * clean up manpage
  * debian dir is not longer included in upstream; create non-native package
  * create symlink rkward.bin.1 to rkward.1 and remove linitian expection (no man page)
  * use ${shlibs:Depends} for runtime dependencies
  * make sure to require correct version of r-base-core
  * specify --with-r-home in ./configure, in case there is more than one version of R installed

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Sun, 23 Apr 2006 19:45:00 +0200

rkward (0.3.5) unstable; urgency=low

  * new upstream release
  * add lintian exception for rkward.bin (no man page)
  * remove call to build-help.pl during postinst postrm
  * use --disable-rpath in ./configure
  * remove /usr/sbin from dirs
  * use standards version 3.6.2
  * add man page
  * remove redundant build-dependency on libqt3-mt-dev
  * break long lines in description

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Sun, 16 Apr 2006 15:45:00 +0200

rkward (0.3.4) unstable; urgency=low

  * new upstream release
  * debian directory is now included in official release

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Tue, 08 Nov 2005 16:00:33 +0200

rkward (0.3.3) unstable; urgency=low

  * new upstream release
  * Adjusted depends for R (2.1.0)
  * added run-time depend for libstdc++6
  * correct path for KDE help files

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Mon, 10 Oct 2005 11:20:33 +0200

rkward (0.3.2) unstable; urgency=low

  * new upstream release
  * Initial Release.
  * removing r.xml highlighting definition in order to avoid conflict with verison shipped with katepart

 -- Thomas Friedrichsmeier <tfry@users.sourceforge.net>  Tue, 26 Apr 2005 00:00:33 +0200
